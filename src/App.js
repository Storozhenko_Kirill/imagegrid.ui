import React from 'react';
import './App.css';

import NewImageFormContainer from "./components/NewIMageForm/NewImageFormContainer";
import ImageGridContainer from "./components/Grid/ImageGridContainer";

function App() {
  return (
    <div>
      <NewImageFormContainer/>
      <ImageGridContainer/>
    </div>
  );
}

export default App;

export const Service = (() => {
  let storageData = JSON.parse(localStorage.getItem('images'));

  return {
    getImages: () => {
      return new Promise((resolve, reject) => {
        if(storageData === null) {
           reject({statusCode: 404, message: 'Not Found'});
        }

        resolve(storageData);
      })
    },

    setImage: (img) => {
      if (storageData === null) {
        storageData = []
      }

      storageData.push(img);

      localStorage.setItem('images', JSON.stringify(storageData));

      return new Promise(resolve => {
        resolve({statusCode: 201})
      })
    },

    deleteImage: (id) => {
      storageData = storageData.filter(image => image.id !== id);

      localStorage.setItem('images', JSON.stringify(storageData));

      return new Promise(resolve => {
        resolve({statusCode: 200});
      });
    },

    updateImage: (id, src) => {
      storageData.map(image => {
        if (image.id === id) {
          image.src = src;
        }

        return image;
      });

      localStorage.setItem('images', JSON.stringify(storageData));

      return new Promise(resolve => {
        resolve({statusCode: 200})
      })
    },

    saveTooltipSettings: (id, {tooltipText, tooltipPosition, tooltipColor}) => {
      storageData.map(image => {
        if (image.id === id) {
          image.tooltipText = tooltipText;
          image.tooltipPosition = tooltipPosition;
          image.tooltipColor = tooltipColor;
        }
        return image
      });

      localStorage.setItem('images', JSON.stringify(storageData));

      return new Promise(resolve => {
        resolve({statusCode: 200})
      });
    }
  }
})();
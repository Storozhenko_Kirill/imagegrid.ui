import React from 'react'
import {Field, reduxForm} from 'redux-form'

import style from './style/EditView.module.css'


const TooltipFrom = (props) => {
  return (
    <form>
      <p className={style.title}>Tooltip text</p>
      <Field className={style.tooltipTextField}
             name='tooltipText'
             component='input'
             type='text'
             placeholder='Tooltip text'
      />

      <p className={style.title}>Tooltip position</p>
      <label>
        <Field className={style.radioBtn} name='tooltipPosition' component='input' type='radio' value='top'/>
        <span className={style.labelTitle}>top</span>
      </label>
      <label>
        <Field className={style.radioBtn} name='tooltipPosition' component='input' type='radio' value='right'/>
        <span className={style.labelTitle}>right</span>
      </label>
      <label>
        <Field className={style.radioBtn} name='tooltipPosition' component='input' type='radio' value='bottom'
        />
        <span className={style.labelTitle}>bottom</span>
      </label>
      <label>
        <Field className={style.radioBtn} name='tooltipPosition' component='input' type='radio' value='left'/>
        <span className={style.labelTitle}>left</span>
      </label>

      <p className={style.title}>Tooltip color</p>
      <label>
        <Field className={style.radioBtn} name='tooltipColor' component='input' type='color'/>
      </label>
      <div className={style.btnRow}>
        <label className={style.button} htmlFor='updateImage'>
          Update image
          <input id='updateImage' onChange={(e) => props.onUpdateImage(e, props.options.id)} type="file"/>
        </label>
        <button className={style.button} onClick={() => props.onDeleteImage(props.options.id)} type='button'>Delete</button>
        <button className={style.button} onClick={props.onHideEditMode} type='button'>Close</button>
        <button className={style.button} onClick={(e) => props.onEditTooltip(e, props.id)}  type='submit'>Save and close</button>
      </div>
    </form>
  )
};

const TooltipReduxForm = reduxForm({
  form: 'tooltipForm',
})(TooltipFrom);


const EditView = (props) => {
  let initialValues = {
    tooltipText: props.options.tooltipText,
    tooltipPosition: props.options.tooltipPosition,
    tooltipColor: props.options.tooltipColor
  };

  return (
    <div className={style.editContainer}>
      <div className={style.editPosition}>
        <div className={style.editBody}>
          <img
            className={style.image}
            src={props.options.src}
            alt=""
          />
          <div>
            <TooltipReduxForm
              onUpdateImage={props.onUpdateImage}
              onDeleteImage={props.onDeleteImage}
              onHideEditMode={props.onHideEditMode}
              onEditTooltip={props.onEditTooltip}
              id={props.options.id}
              options={props.options}
              initialValues={initialValues}
            />
          </div>
        </div>
      </div>
    </div>
  )
};

export default EditView;
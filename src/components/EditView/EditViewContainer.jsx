import React, {Component} from 'react'
import EditView from "./EditView";
import {connect} from "react-redux";
import {editView} from "../../redux/edit-selectors";
import {onDeleteImage, onEditTooltip, onUpdateImage} from "../../redux/image-reducer";
import {imageToBase64} from "../../helpers/imageToBase64";

class EditViewContainer extends Component {
  handleDelete = (id) => {
    this.props.onHideEditMode();
    this.props.onDeleteImage(id)
  };

  handleUpdate = async (e, id) => {
    let base64 = await imageToBase64(e);

    this.props.onUpdateImage(id, base64);
  };

  handleClose = () => {
    this.props.onHideEditMode();
  };

  handleSubmitTooltip = (e, id) => {
    e.preventDefault();
    this.props.onHideEditMode();
    this.props.onEditTooltip(id, this.props.tooltipForm.values);
  };

  render() {
    return (
      <EditView
        options={this.props.options}
        onDeleteImage={this.handleDelete}
        onUpdateImage={this.handleUpdate}
        onHideEditMode={this.handleClose}
        onEditTooltip={this.handleSubmitTooltip}
      />
    )
  }
}

let mapStateToProps = (state) => {
  return {
    options: editView(state),
    tooltipForm: state.form.tooltipForm
  }
};

export default connect(mapStateToProps, {
  onDeleteImage, onUpdateImage, onEditTooltip
})(EditViewContainer);
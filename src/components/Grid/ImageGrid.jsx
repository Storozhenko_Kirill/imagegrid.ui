import React, {useState} from 'react'

import style from './style/ImageGrid.module.css'

import ImageItem from "./ImageItem";
import EditViewContainer from "../EditView/EditViewContainer";

const ImageGrid = (props) => {
  const [editMode, setEditMode] = useState(false);
  const showEditMode = (id) => {
    setEditMode(true);

    props.handleOpenEdit(id);
  };

  const hideEditMode = () => {
    setEditMode(false);
  };

  return (
    <>
      <div className={style.grid}>
        {
          !props.images.length
            ? <p>Not found</p>
            : props.images.map(image => {
                return (
                  <ImageItem
                    key={image.id}
                    imageInfo={image}
                    onShowEditMode={showEditMode}
                  />
                )
              })
        }
      </div>

      {editMode && <EditViewContainer onHideEditMode={hideEditMode}/>}
    </>
  )
};

export default ImageGrid;
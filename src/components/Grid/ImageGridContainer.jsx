import React, {Component} from 'react'
import {connect} from "react-redux";

import ImageGrid from "./ImageGrid";
import {getImages} from "../../redux/image-reducer";
import {openEdit} from "../../redux/edit-reducer";

class ImageGridContainer extends Component {
  handleOpenEdit = (id) => {
    this.props.openEdit(id);
  };

  componentDidMount() {
    this.props.getImages();
  }

  render() {
    return (
      <ImageGrid images={this.props.images} handleOpenEdit={this.handleOpenEdit}/>
    )
  }
}

let mapStateToProps = (state) => {
  return {
    images: state.images.images
  }
};

export default connect(mapStateToProps, {getImages, openEdit})(ImageGridContainer)
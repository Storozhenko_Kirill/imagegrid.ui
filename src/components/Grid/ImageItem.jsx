import React from 'react'

import style from './style/ImageItem.module.css'

import Tooltip from "../Tooltip/Tooltip";

const ImageItem = (props) => {
  console.log();
  return (
    <div className={style.imageContainer}
         onClick={() => props.onShowEditMode(props.imageInfo.id)}
    >
      <img
        src={props.imageInfo.src}
        alt=""
      />
      {
      props.imageInfo.tooltipText
        ? <Tooltip position={props.imageInfo.tooltipPosition}
                   text={props.imageInfo.tooltipText}
                   visiBilityClass={style.tooltipVisibility}
                   color={props.imageInfo.tooltipColor}
        />
        : null
    }


    </div>
  )
};

export default ImageItem
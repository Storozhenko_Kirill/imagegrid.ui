import React from 'react'
import style from './style/NewImageFrom.module.css'

const NewImageForm = (props) => {
  return (
    <form className={style.form}>
      <label
        className={style.formLabel}
        htmlFor="uploadImage">
        Upload Image
      </label>
      <input
        id="uploadImage"
        className={style.formInput}
        type="file"
        onChange={props.addNewImage}
       />
    </form>
  )
};

export default React.memo(NewImageForm);
import React, {Component} from 'react'
import {connect} from "react-redux";
import {addImage} from "../../redux/image-reducer";

import NewImageForm from "./NewImageForm";
import {imageToBase64} from "../../helpers/imageToBase64";


class NewImageFormContainer extends Component {
  handleChangeInput = async (e) => {
    let base64 = await imageToBase64(e);

    this.props.addImage(base64);
  };

  render() {
    return (
      <NewImageForm addNewImage={this.handleChangeInput}/>
    )
  }
}

export default connect(null, {addImage})(NewImageFormContainer);
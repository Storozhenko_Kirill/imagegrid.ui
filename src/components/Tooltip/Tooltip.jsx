import React from 'react'
import style from './style/Tooltip.module.css'

const Tooltip = (props) => {
  let position = props.position;
  let stylePosition;

  if (position === 'top') {
    stylePosition = style.positionTop
  } else if (position === 'bottom') {
    stylePosition = style.positionBottom
  } else if (position === 'left') {
    stylePosition = style.positionLeft
  } else {
    stylePosition = style.positionRight
  }

  return (
    <div
      style={{backgroundColor: props.color}}
      className={style.imageTooltip + ' ' + stylePosition + ' ' + props.visiBilityClass}
    >
      {props.text}
    </div>
  )
};

export default Tooltip;
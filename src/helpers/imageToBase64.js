export const imageToBase64 = (e) => {
  let target = e.target;
  let file = target.files[0];
  let reader = new FileReader();

  return new Promise((resolve, reject) => {
    reader.onloadend = () => {
      resolve(reader.result);
    };

    reader.onerror = reject;

    if (file) {
      reader.readAsDataURL(file);
    }
  });
};
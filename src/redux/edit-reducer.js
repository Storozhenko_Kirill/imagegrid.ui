const OPEN_EDIT_VIEW = 'OPEN_EDIT_VIEW';

const initialState = {
  editOption: {
    id: '',
  }
};

const editReducer = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_EDIT_VIEW:
      return {
        ...state,
        editOption: {
          ...state.editOption,
          id: action.id
        }
      };
    default:
      return state;
  }
};

export const openEdit = (id) => {
  return {
    type: OPEN_EDIT_VIEW,
    id
  }
};

export default editReducer;
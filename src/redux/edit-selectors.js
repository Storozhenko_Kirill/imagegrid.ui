export const editView = (state) => {
  const images = state.images.images;
  const editId = state.editOption.editOption.id;

  return images.find(image => image.id === editId);
};
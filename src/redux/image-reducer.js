import {Service} from "../api/service";

const ADD_NEW_IMAGE = 'ADD_NEW_IMAGE';
const SET_IMAGES = 'SET_IMAGES';
const DELETE_IMAGE = 'DELETE_IMAGE';
const UPDATE_IMAGE = 'UPDATE_IMAGE';
const EDIT_TOOLTIP = 'EDIT_TOOLTIP';

const initialState = {
  images: []
};

const imageReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_IMAGES:
      return {
        ...state,
        images: [
          ...action.images
        ]
      };
    case ADD_NEW_IMAGE:
      return {
        ...state,
        images: [
          ...state.images,
          {
            id: action.id,
            src: action.src
          }
        ]
      };
    case DELETE_IMAGE:
      return {
        ...state,
        images: state.images.filter(image => image.id !== action.id)
      };
    case UPDATE_IMAGE:
      return {
        ...state,
        images: [
          ...state.images.map(image => {
            if(image.id === action.id) {
              image.src = action.src
            }

            return image
          })
        ]
      };
    case EDIT_TOOLTIP:
      return {
        ...state,
        image: [
          ...state.images.map(image => {
            if(image.id === action.id){
              image.tooltipText = action.tooltipText;
              image.tooltipPosition = action.tooltipPosition;
              image.tooltipColor = action.tooltipColor;
            }

            return image
          })
        ]
      };
    default:
      return state;
  }
};

let ID = () => '_' + Math.random().toString(36).substr(2, 9);
/*Action Creators*/
const setImages = (images) => {
  return {
    type: SET_IMAGES,
    images
  }
};
const addNewImage = ({id, src}) => {
  return {
    type: ADD_NEW_IMAGE,
    id,
    src,
  }
};
const deleteImage = (id) => {
  return {
    type: DELETE_IMAGE,
    id
  }
};
const updateImage = (id, src) => {
  return {
    type: UPDATE_IMAGE,
    id,
    src
  }
};
const editTooltip = (id, {tooltipText, tooltipPosition, tooltipColor}) => {
  return {
    type: EDIT_TOOLTIP,
    id,
    tooltipText,
    tooltipPosition,
    tooltipColor
  }
};

/*Thunk Creators*/
export const getImages = () => async (dispatch) => {
  let response = await Service.getImages();

  if (response.statusCode === 404) {
    return false;
  }

  dispatch(setImages(response));
};
export const addImage = (imgSrc) => async (dispatch) => {
  let img = {
    id: ID(),
    src: imgSrc,
    tooltipText: null

  };

  let response = await Service.setImage(img);

  if (response.statusCode === 201) {
    dispatch(addNewImage(img));
  }
};
export const onDeleteImage = (id) => async (dispatch) => {
  let response = await Service.deleteImage(id);

  if(response.statusCode === 200) {
    dispatch(deleteImage(id));
  }
};
export const onUpdateImage = (id, src) => async (dispatch) => {
  let response = await Service.updateImage(id, src);

  if(response.statusCode === 200) {
    dispatch(updateImage(id, src))
  }
};
export const onEditTooltip = (id, {tooltipText, tooltipPosition, tooltipColor}) => async (dispatch) => {
  let response = await Service.saveTooltipSettings(id, {tooltipText, tooltipPosition, tooltipColor});

  if (response.statusCode === 200) {
    dispatch(editTooltip(id, {tooltipText, tooltipPosition, tooltipColor}))
  }
};

export default imageReducer;
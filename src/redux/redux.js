import {combineReducers, createStore, applyMiddleware} from "redux";
import thunk from 'redux-thunk';
import imageReducer from "./image-reducer";
import editReducer from "./edit-reducer";
import { reducer as formReducer } from 'redux-form'

let reducers = combineReducers({
  images: imageReducer,
  editOption: editReducer,
  form: formReducer,
});

let store = createStore(reducers, applyMiddleware(thunk));

export default store;